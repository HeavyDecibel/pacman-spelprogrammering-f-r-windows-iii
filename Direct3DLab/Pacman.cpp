#include "Pacman.h"
#include "Sprite.h"
#include "Timer.h"

Pacman::Pacman(DirectX *directX) :MovingObject(directX)
{
	
	//Pacman skapas upp med sprite och liv etc.
	
	sprite = new Sprite(30, 30, 0, 0, 273, 449, 3, 1, 4, directX, "Graphics\\PacmanSprites.png");
	sprite->setTimer(100);
	speed = 4.0f;
	lives = 3;
	score = 0;
	state = NORMAL;
	
}

Pacman::~Pacman()
{
}

void Pacman::Draw(){
	sprite->animateSprite();
	MovingObject::Draw();
}

void Pacman::Move(){
	
	//Roterar pacman-spriten s� den alltid pekar �t det h�llet den g�r
	switch (direction){
		case UP:
			sprite->rotateSprite(270);
			break;
		case DOWN:
			sprite->rotateSprite(90);
			break;
		case LEFT:
			sprite->rotateSprite(180);
			break;
		case RIGHT:
			sprite->rotateSprite(0);
			break;
	}

	MovingObject::Move();
}


//Returnerar privata medlemmar.
int Pacman::GetScore(){
	return score;
}

int Pacman::GetLives(){
	return lives;
}

void Pacman::AddScore(int score){
	this->score += score;
}

void Pacman::ReduceLives(){
	lives--;
}

void Pacman::Reset(){
	sprite->setNewPos(273, 449);
	direction = IDLE;
}

PacmanState Pacman::GetState(){
	return state;
}

void Pacman::SetInvincible(bool onoff){
	if (onoff == true)
		state = INVINCIBLE;
	else
		state = NORMAL;
}

void Pacman::SetDead(bool onoff){
	if (onoff == true)
		state = EATEN;
	else
		state = NORMAL;
}