math.randomseed(os.time())

--Det största scriptet, de bestämer åt vilket håll spöket ska beroende på flera faktorer.
--Riktningen kan antingen slumpas ut eller röra spöket mot ett mål. Tyvärr blir det inte alltid närmsta
--vägen, men jag fick inte riktigt ihop det fullt ut där. Som tur är så fungerar några av de riktiga 
--Pacman-versionerna lika illa där :-)

function SetNewDirection(ghostX, ghostY, north, south, west, east, ghostPattern, ghostDirection, pacmanX, pacmanY, pacmanDirection)
	--Kör math.random() tre gånger, annars kommer alltid samma slumptal genereras av någon anledning
	dofile("Enums.lua")
  
  print("Startar scriptet och sår ett slumpfrö")
  
  math.random()
	math.random()
	math.random()
  
	local randomDirection
	local directionSet=false
	
  local targetX
  local targetY
  
	--Om spöket befinner sig i boet så slumpas inte riktningen utan den
	--lämnar istället boet. Annars kan det irra runt där i all evighet
	if(ghostX==17 and ghostY==17)
	then
		randomDirection=Directions["UP"]
		directionSet=true
	end
		
  
  --Om spöket ska gå till boet så sätts målet till koordinaten i boet
  if(ghostPattern==GhostPatterns["GO_HOME"])
  then
    targetX=17
    targetY=17
  end
  
  --Om spöket ska jag Pacman sätts Pacmans koordinater som målpunkt
  if(ghostPattern==GhostPatterns["PURSUIT"])
  then  
    targetX=pacmanX
    targetY=pacmanY
  end

  --Om spöket ska undvika Pacman så sätts målpunkten till motsatta hörnet från Pacman
  if(ghostPattern==GhostPatterns["EVADE"])
  then
    if(pacmanX<=17)
    then    
      targetX=35
    else
      targetX=0
    end
    
    if(pacmanY<19)
    then
      targetY=39
    else
      targetY=0
    end
  end
  
  --För att spöket inte ska fastna i en slinga mellan två punkter så förbjuds det att gå tillbaka samma väg det kom
  if(ghostPattern==GhostPatterns["GO_HOME"] or ghostPattern==GhostPatterns["PATROL"])
    then
      if(ghostDirection==Directions["DOWN"])
      then
        north=false
      end    
      
      if(ghostDirection==Directions["UP"])
      then
        south=false
      end
      
      if(ghostDirection==Directions["LEFT"])
      then
        east=false        
      end
      
      if(ghostDirection==Directions["RIGHT"])
      then
        west=false       
      end      
    end
  
	--Om spöket har en målpunkt det ska nå så räknas offsetsen, DeltaX och DeltaY, ut till målet och därefter tas beslut åt vilket håll det ska gå
  if(ghostPattern==GhostPatterns["GO_HOME"] or ghostPattern==GhostPatterns["PURSUIT"] or ghostPattern==GhostPatterns["EVADE"])
	then  
    
    local deltaX=ghostX-targetX
    local deltaY=ghostY-targetY

    local tmpDY
    local tmpDX

    --Omvandlar deltaX och deltaY till positivta tal för att
    --kunna se vilket som är störst senare
    if deltaX<0
    then
      tmpDX=(deltaX*(-1))
    else
      tmpDX=deltaX
    end

    if deltaY<0
    then
      tmpDY=(deltaY*(-1))
    else
      tmpDY=deltaY
    end
     
    print("*************************************")
    print(deltaX)
    print(deltaY)
    print(tmpDX)
    print(tmpDY)
    print("*************************************")
         
    print("90: Försöker nå målet!")
    --Kollar om Y är större eller lika med X.
		if(tmpDY>=tmpDX)
		then
			print("94: Y är större än X - försöker flytta vertikalt")
      --Om Y är positivt så vill vi få spöket att gå upp, då deltaY är större än målpunkten
			if(deltaY>=0)
			then				
				print("98: Y är positivt - försöker flytta Uppåt")
        if(north==true)
				then					
          randomDirection=Directions["UP"]
					directionSet=true
				else
					print("104: Gick inte att flytta Uppåt - försöker i sidled")
          --Går det inte att röra sig Uppåt så kontrolleras om spöket ska gå åt höger
					--eller vänster.
					--Är deltaX positivt så ska spöket röra sig åt vänster först och främst.
					if(deltaX>0)
					then
						print("110: X är positivt - försöker flytta vänster")
            --Vänster har prioritet
						if(west==true)
						then
							randomDirection=Directions["LEFT"]
							directionSet=true
						else
							--Går inte det så får det röra sig åt höger istället. 
							print("118: Gick inte att flytta vänster - flyttar höger")
              if(east==true)
              then                
                randomDirection=Directions["RIGHT"]
                directionSet=true
              end              
						end
					else
						print("123: X är negativt - försöker flytta höger")
            --Är deltaX negativt (eller 0) så ska spöket flytta sig åt höger i första hand.
						if(east==true)
						then
							randomDirection=Directions["RIGHT"]
							directionSet=true
						else
							print("130: Gick inte att flytta höger - flyttar vänster")
              if(west==true)
              then
                randomDirection=Directions["LEFT"]
                directionSet=true
              end
            end
					end
				end
			else
				print("137: Y är negativt - försöker flytta neråt")
        --Om Y är negativt eller 0 (men då borde X ha företräde oavsett) så
				--ska spöket flyttas neråt. 
				if(south==true)
				then
					randomDirection=Directions["DOWN"]
					directionSet=true			
				else
					print("145: Gick inte att flytta nedåt - försöker i sidled")
          --Går det inte att flytta neråt så ska spöket flyttas i sidled, precis som ovan.
					if(deltaX>0)
					then
						print("149: X är positivt - försöker flytta vänster")
            --Vänster har prioritet
						if(west==true and ghostDirection~=Directions["RIGHT"])
						then
							randomDirection=Directions["LEFT"]
							directionSet=true
						else
							print("156: Gick inte att flytta vänster - flyttar höger")
              if(east==true)
              then
                randomDirection=Directions["RIGHT"]
                directionSet=true
              end
            end
					else
						print("161: X är negativt - försöker flytta höger")
            --Höger har prioritet
						if(east==true and ghostDirection~=Directions["LEFT"])
						then
							randomDirection=Directions["RIGHT"]
							directionSet=true
						else
							print("168: Gick inte att flytta höger - flyttar vänster")
              if(west==true)
              then
                randomDirection=Directions["LEFT"]
                directionSet=true
              end
						end
					end
				end  
      end
		else
			--Om nu deltaX är större, dvs spöket har längre i sidled än i vertikalled till målet
      --så försöker vi flytta spöket horisontellt istället för att närma oss målet den vägen.
      print("179: X är större än Y - försöker flytta i horisontellt")
      if(deltaX>0)
			then
				--Om deltaX är positivt så flyttar vi spöket åt vänster
				print("183: X är positivt - försöker flytta vänster")
        if(west==true and ghostDirection~=Directions["RIGHT"])
				then
					randomDirection=Directions["LEFT"]
					directionSet=true
				else
          print("189: Gick inte att flytta vänster - kollar vertikalt")
          if(deltaY>=0)
          then
            print("192: Y är positivt - försöker flytta Uppåt")
            if(north==true)
            then
                randomDirection=Directions["UP"]
                directionSet=true
            else
              print("198: Gick inte att flytta Uppåt - flyttar höger")
              if(east==true)
              then
                randomDirection=Directions["RIGHT"]
                directionSet=true
              end
            end
          else
            print("203: Y är negativt - försöker flytta nedåt")
            if(south==true)
            then
              randomDirection=Directions["DOWN"]
              directionSet=true
            else
              print("209: Gick inte att flytta nedåt - flyttar höger")
              if(east==true)
              then
                randomDirection=Directions["RIGHT"]
                directionSet=true
              end
            end
          end
        end        
      else
				--Är deltaX 0 eller mindre så flyttar vi spöket åt höger, i övrigt är det som sekvensen ovan.
        print("217: X är negativt - försöker flytta höger")
        if(east==true and ghostDirection~=Directions["LEFT"])
				then
					randomDirection=Directions["RIGHT"]
					directionSet=true
				else
          print("223: Gick inte att flytta höger - kollar vertikalt")
          if(deltaY>=0)
          then
            print("226: Y är positivt - försöker flytta Uppåt")
            if(north==true)
            then
                randomDirection=Directions["UP"]
                directionSet=true
            else
              print("232: Gick inte att flytta Uppåt - flyttar vänster")
              if(west==true)
              then
                randomDirection=Directions["LEFT"]
                directionSet=true
              end
            end
          else
            print("237: Y är negativt - försöker flytta nedåt")
            if(south==true)
            then
              randomDirection=Directions["DOWN"]
              directionSet=true
            else
              print("243: Gick inte att flytta nedåt - flyttar vänster")
              if(west==true)
              then
                randomDirection=Directions["LEFT"]
                directionSet=true
              end
            end
          end
        end  
			end
		end
	end
	
  --Om soöket är i boet så får det inte gå åt sidan. Då kan det fastna där, utan måste gå uppåt.
  if(ghostX==17 and ghostY==17)
  then
    if(ghostPattern==GhostPatterns["GO_HOME"])
    then
      randomDirection=Directions["IDLE"]
      directionSet=true
    end
    
    if(ghostPattern==GhostPatterns["PURSUIT"])
    then
      randomDirection=Directions["UP"]
      directionSet=true
    end	
  end
  
  if(ghostX==17 and ghostY==14 and ghostPattern==GhostPatterns["PURSUIT"])
  then
    directionSet=false
  end
  
  
  --Går det inte att sätta riktningen enligt reglerna ovan så slumpas en riktning ut.
  if(directionSet==false)
  then
    print("250: Slumpar ut en riktning")
    
    --Om spöket försöker fly från Pacman så sätts Pacmans riktning som en förbjuden väg
    if(ghostPattern==GhostPatterns["FLEE"])
    then
      if(pacmanDirection==Directions["UP"])
      then
        north=false
      end    
      
      if(pacmanDirection==Directions["DOWN"])
      then
        south=false
      end
      
      if(pacmanDirection==Directions["RIGHT"])
      then
        east=false        
      end
      
      if(pacmanDirection==Directions["LEFT"])
      then
        west=false        
      end      
    end
  end
  
    
	
  --Sätter en ny riktning beroende på om det finns väggar i vägen eller inte.
	--Finns en liten risk att loopen tar för lång tid, om man har otur och den slumpar fel nummer
	while(directionSet==false)
	do  
    randomDirection=math.random(1,4)
		
		if(randomDirection==1)
		then
			randomDirection=Directions["UP"]
		end
		
		if(randomDirection==2)
		then
			randomDirection=Directions["DOWN"]
		end
		
		if(randomDirection==3)
		then
			randomDirection=Directions["LEFT"]
		end
		
		if(randomDirection==4)
		then
			randomDirection=Directions["RIGHT"]
		end
		
		if(randomDirection==Directions["UP"])
		then
			if(north==true)
			then
				directionSet=true
			end
		end
		
		if(randomDirection==Directions["DOWN"])
		then
			if(south==true)
			then
				directionSet=true
			end
		end
		
		if(randomDirection==Directions["LEFT"])
		then
			if(west==true)
			then
				directionSet=true
			end
		end
		
		if(randomDirection==Directions["RIGHT"])
		then
			if(east==true)
			then
				directionSet=true
			end
		end
	end
	
	return randomDirection
end
