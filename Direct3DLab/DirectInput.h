#pragma once

#ifndef __DIRECTINPUT_H_INCLUDED__
#define __DIRECTINPUT_H_INCLUDED__

#include <dinput.h>

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

class DirectInput
{
private:
	//DirectInput-objekt
	LPDIRECTINPUT8 directInputInterface; //Gr�nsnittet
	LPDIRECTINPUTDEVICE8 directInputMouse; //Musenhet (kommer inte anv�ndas i spelet, bara f�r menyn)
	LPDIRECTINPUTDEVICE8 directInputKeyboard; //Tangentbordsenhet
	DIMOUSESTATE mouseState; //F�r att returnera musens position, knapptryck etc
	char keys[256]; //Upps�ttning tangenter
	int keyPressState[256];
	bool directInputInit(HWND);

	//Muskoordinater, anv�nds inte i det h�r projektet
	int mousePosX = 0;
	int mousePosY = 0;


public:

	//Konstruktor och destruktor
	DirectInput(HWND window);
	~DirectInput();

	void directInputUpdate(HWND window); //<-Uppdaterar inputenheterna
	void directInputShutdown(); //<-St�nger ner inputenheterna
	int keyDown(int); //<-Kollar om en tangent h�lls ned
	bool keyPressed(DWORD key); //<-Kollar om en tangent trycks ned
	int mouseButton(int); //<-Kollar om en musknapp trycks p�
	int mouseX(); //<-G�r musens x...
	int mouseY(); //...och y-koordinater synliga och skrivbara
	POINT mousePosition(HWND window); //<-Returnerar muspositionen som en point
	bool DirectInput::keyUp(DWORD key); //<-Kollar om en tangent sl�pps upp
	void poll(HWND window); //<-Pollar mus och t-bord

};

#endif

