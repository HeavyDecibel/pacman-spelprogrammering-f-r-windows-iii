#pragma once
#include "MovingObject.h"
#include "Enums.h"


class Pacman :
	public MovingObject
{
private:
	int lives;
	int score;
	PacmanState state;

public:
	Pacman(DirectX *directX);
	~Pacman();

	virtual void Move() override;
	virtual void Draw() override;
	int GetLives();
	int GetScore();
	void AddScore(int score);
	void ReduceLives();
	void Reset();
	PacmanState GetState();
	void SetInvincible(bool onoff);
	void SetDead(bool onoff);

};

