math.randomseed(os.time())

function SetNewDirection(ghostX, ghostY, north1, north2, south1, south2, west1, west2, east1, east2)
	--Kör math.random() tre gånger, annars kommer alltid samma slumptal genereras av någon anledning
	math.random()
	math.random()
	math.random()
	
	local randomDirection
	local directionSet=false
	
	--Om spöket befinner sig i boet så slumpas inte riktningen utan den
	--lämnar istället boet. Annars kan det irra runt där i all evighet
	if(ghostX==17 and ghostY==17)
	then
		randomDirection="Up"
		directionSet=true
	end
	
	
	--Sätter en ny riktning beroende på om det finns väggar i vägen eller inte.
	--Finns en liten risk att loopen tar för lång tid, om man har otur och den slumpar fel nummer
	while(directionSet==false)
	do
		randomDirection=math.random(1,4)
		
		if(randomDirection==1)
		then
			randomDirection="Up"
		end
		
		if(randomDirection==2)
		then
			randomDirection="Down"
		end
		
		if(randomDirection==3)
		then
			randomDirection="Left"
		end
		
		if(randomDirection==4)
		then
			randomDirection="Right"
		end
		
		if(randomDirection=="Up")
		then
			if(north1==99 or north1==91)
			then
				if(north2==99 or north2==91)
				then
					directionSet=true
				end
			end
		end
		
		if(randomDirection=="Down")
		then
			if(south1==99 or south1==91)
			then
				if(south2==99 or south2==91)
				then
					directionSet=true
				end
			end
		end
		
		if(randomDirection=="Left")
		then
			if(west1==99 or west1==91)
			then
				if(west2==99 or west2==91)
				then
					directionSet=true
				end
			end
		end
		
		if(randomDirection=="Right")
		then
			if(east1==99 or east1==91)
			then
				if(east2==99 or east2==91)
				then
					directionSet=true
				end
			end
		end
	end
	
	return randomDirection
end