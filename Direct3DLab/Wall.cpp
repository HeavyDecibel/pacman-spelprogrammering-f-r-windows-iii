#include "Wall.h"
#include "Sprite.h"


Wall::Wall(DirectX *directX):StationaryObject(directX)
{
	type = WALL;
	sprite = new Sprite(16, 16, 0, 0, 0, 0, 36, 6, 6, directX, "Graphics\\WallSprites.png");
}


Wall::~Wall()
{
}

