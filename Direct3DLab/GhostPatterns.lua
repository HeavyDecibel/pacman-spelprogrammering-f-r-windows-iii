function SetNewPattern(ghostState, distanceToPacman)
  
  --Script som sätter spökets rörelsemönster beroende på state och avstånd till Pacman
  
	dofile("Enums.lua")

  print("Sätter spökpattern")
  
  if(ghostState==GhostStates["DEAD"])
	then
		return GhostPatterns["GO_HOME"]
	end
	
  if(ghostState==GhostStates["WANDER"] and distanceToPacman<10)
  then
    return GhostPatterns["PURSUIT"]
  end
  
  if(ghostState==GhostStates["WANDER"])
  then
    return GhostPatterns["PATROL"]
  end
  
  if(ghostState==GhostStates["ELIMINATE"])
  then
    return GhostPatterns["PURSUIT"]
  end
  
  if(ghostState==GhostStates["SCARED"] and distanceToPacman<10)
  then
    return GhostPatterns["FLEE"]
  end
  
  if(ghostState==GhostStates["SCARED"])
  then
    return GhostPatterns["EVADE"]
  end

  print("Hamnar vi här har något gått fel!")

end
