//Basklass f�r fasta objekt som v�ggar, po�ngpluppar, powerups etc
//Vissa objekt kan bli upp�tna och har d�rf�r en flagga f�r det som om den �r satt 
//f�rhindrar att det blir utritat och att man kan krocka med det igen. Pip, dvs de vita plupparna 
//och power ups �rver h�r ifr�n. Likas� walls.

#include "StationaryObject.h"
#include "Sprite.h"

StationaryObject::StationaryObject(DirectX *directX):
directX(directX)
{
	eaten = false;
}


StationaryObject::~StationaryObject()
{
	delete sprite;
}

void StationaryObject::Draw(){
	if (!eaten)
		sprite->drawSpriteV2();
}

StationaryType StationaryObject::GetType(){
	return type;
}

Sprite* StationaryObject::GetSprite(){
	return sprite;
}

void StationaryObject::SetFrame(int frame){
	sprite->setFrame(frame);
}

void StationaryObject::SetPosition(float x, float y){
	sprite->setNewPos(x, y);
}

void StationaryObject::SetEaten(){
	eaten = true;
}

int StationaryObject::GetScore(){
	return scoreValue;
}

bool StationaryObject::IsEaten(){
	return eaten;
}