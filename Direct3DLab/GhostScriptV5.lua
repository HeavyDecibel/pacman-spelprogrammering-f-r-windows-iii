math.randomseed(os.time())

GhostStates={
	WANDER=0,
	SCARED=1,
	DEAD=2,
	ELIMINATE=3
}

GhostPatterns={
	PATROL=0,
	PURSUIT=1,
	EVADE=2,
	FLEE=3,
	GO_HOME=4
}

PacmanStates={
  NORMAL=0,
	INVINCIBLE=1,
	EATEN=2
}

Directions={
  UP=0,
	DOWN=1,
	LEFT=2,
	RIGHT=3,
	IDLE=4
}

function SetNewDirection(ghostX, ghostY, north, south, west, east, ghostPattern, ghostDirection, pacmanX, pacmanY, pacmanDirection)
	--Kör math.random() tre gånger, annars kommer alltid samma slumptal genereras av någon anledning
	math.random()
	math.random()
	math.random()
	
	local randomDirection
	local directionSet=false
	
  local targetX
  local targetY
  
	--Om spöket befinner sig i boet så slumpas inte riktningen utan den
	--lämnar istället boet. Annars kan det irra runt där i all evighet
	if(ghostX==17 and ghostY==17)
	then
		randomDirection="Up"
		directionSet=true
	end
		
  if(ghostPattern==GhostPatterns["GO_HOME"])
  then
    targetX=17
    targetY=17
  end
  
  if(ghostPattern==GhostPatterns["PURSUIT"])
  then  
    targetX=pacmanX
    targetY=pacmanY
  end

  if(ghostPattern==GhostPatterns["EVADE"])
  then
    if(pacmanX<=17)
    then    
      targetX=35
    else
      targetX=0
    end
    
    if(pacmanY<19)
    then
      targetY=39
    else
      targetY=0
    end
  end
  
	if(ghostPattern==GhostPatterns["GO_HOME"] or ghostPattern==GhostPatterns["PURSUIT"] or ghostPattern==GhostPatterns["EVADE"])
	then  
    
    local deltaX=ghostX-targetX
    local deltaY=ghostY-targetY

    local tmpDY
    local tmpDX

    --Omvandlar deltaX och deltaY till positivta tal för att
    --kunna se vilket som är störst senare
    if deltaX<0
    then
      tmpDX=(deltaX*(-1))
    else
      tmpDX=deltaX
    end

    if deltaY<0
    then
      tmpDY=(deltaY*(-1))
    else
      tmpDY=deltaY
    end
     
    print("*************************************")
    print(deltaX)
    print(deltaY)
    print(tmpDX)
    print(tmpDY)
    print("*************************************")
         
    print("90: Försöker hitta hem!")
    --Kollar om Y är större eller lika med X enligt steg 3 ovan.
		if(tmpDY>=tmpDX)
		then
			print("94: Y är större än X - försöker flytta vertikalt")
      --Om Y är positivt så vill vi få spöket att gå upp, då deltaY är större än målpunkten
			if(deltaY>=0)
			then				
				print("98: Y är positivt - försöker flytta uppåt")
        if(north==true)
				then					
          randomDirection="Up"
					directionSet=true
				else
					print("104: Gick inte att flytta uppåt - försöker i sidled")
          --Går det inte att röra sig uppåt så kontrolleras om spöket ska gå åt höger
					--eller vänster enligt punkt 4.
					--Är deltaX positivt så ska spöket röra sig åt vänster först och främst.
					if(deltaX>0)
					then
						print("110: X är positivt - försöker flytta vänster")
            --Vänster har prioritet
						if(west==true)
						then
							randomDirection="Left"
							directionSet=true
						else
							--Går inte det så får det röra sig åt höger istället. 
							print("118: Gick inte att flytta vänster - flyttar höger")
              randomDirection="Right"
							directionSet=true
						end
					else
						print("123: X är negativt - försöker flytta höger")
            --Är deltaX negativt (eller 0) så ska spöket flytta sig åt höger i första hand.
						if(east==true)
						then
							randomDirection="Right"
							directionSet=true
						else
							print("130: Gick inte att flytta höger - flyttar vänster")
              randomDirection="Left"
							directionSet=true
						end
					end
				end
			else
				print("137: Y är negativt - försöker flytta neråt")
        --Om Y är negativt eller 0 (men då borde X ha företräde oavsett) så
				--ska spöket flyttas neråt. 
				if(south==true)
				then
					randomDirection="Down"
					directionSet=true			
				else
					print("145: Gick inte att flytta nedåt - försöker i sidled")
          --Går det inte att flytta neråt så ska spöket flyttas i sidled, precis som ovan.
					if(deltaX>0)
					then
						print("149: X är positivt - försöker flytta vänster")
            --Vänster har prioritet
						if(west==true and ghostDirection~=Directions["RIGHT"])
						then
							randomDirection="Left"
							directionSet=true
						else
							print("156: Gick inte att flytta vänster - flyttar höger")
              randomDirection="Right"
							directionSet=true
						end
					else
						print("161: X är negativt - försöker flytta höger")
            --Höger har prioritet
						if(east==true and ghostDirection~=Directions["LEFT"])
						then
							randomDirection="Right"
							directionSet=true
						else
							print("168: Gick inte att flytta höger - flyttar vänster")
              randomDirection="Left"
							directionSet=true
						end
					end
				end  
      end
		else
			--Om nu deltaX är större, dvs spöket har längre i sidled än i vertikalled till målet
      --så försöker vi flytta spöket horisontellt istället för att närma oss målet den vägen, detta 
      --enligt punkt 6 ovan.
      print("179: X är större än Y - försöker flytta i horisontellt")
      if(deltaX>0)
			then
				--Om deltaX är positivt så flyttar vi spöket åt vänster
				print("183: X är positivt - försöker flytta vänster")
        if(west==true and ghostDirection~=Directions["RIGHT"])
				then
					randomDirection="Left"
					directionSet=true
				else
          print("189: Gick inte att flytta vänster - kollar vertikalt")
          if(deltaY>=0)
          then
            print("192: Y är positivt - försöker flytta uppåt")
            if(north==true)
            then
                randomDirection="Up"
                directionSet=true
            else
              print("198: Gick inte att flytta uppåt - flyttar höger")
              randomDirection="Right"
              directionSet=true
            end
          else
            print("203: Y är negativt - försöker flytta nedåt")
            if(south==true)
            then
              randomDirection="Down"
              directionSet=true
            else
              print("209: Gick inte att flytta nedåt - flyttar höger")
              randomDirection="Right"
              directionSet=true
            end
          end
        end        
      else
				--Är deltaX 0 eller mindre så flyttar vi spöket åt höger, i övrigt är det som sekvensen ovan.
        print("217: X är negativt - försöker flytta höger")
        if(east==true and ghostDirection~=Directions["LEFT"])
				then
					randomDirection="Right"
					directionSet=true
				else
          print("223: Gick inte att flytta höger - kollar vertikalt")
          if(deltaY>=0)
          then
            print("226: Y är positivt - försöker flytta uppåt")
            if(north==true)
            then
                randomDirection="Up"
                directionSet=true
            else
              print("232: Gick inte att flytta uppåt - flyttar vänster")
              randomDirection="Left"
              directionSet=true
            end
          else
            print("237: Y är negativt - försöker flytta nedåt")
            if(south==true)
            then
              randomDirection="Down"
              directionSet=true
            else
              print("243: Gick inte att flytta nedåt - flyttar vänster")
              randomDirection="Left"
              directionSet=true
            end
          end
        end  
			end
		end
	end
	
  if(ghostX==17 and ghostY==17)
  then
    if(ghostPattern==GhostPatterns["GO_HOME"])
    then
      randomDirection="Idle"
      directionSet=true
    end
    
    if(ghostPattern==GhostPatterns["PURSUIT"])
    then
      randomDirection="Up"
      directionSet=true
    end	
  end
  
  if(ghostX==17 and ghostY==14 and ghostPattern==GhostPatterns["PURSUIT"])
  then
    directionSet=false
  end
  
  
  
  if(directionSet==false)
  then
    print("250: Ger upp och slumpar en riktning istället...")
    
    --Om spöket försöker fly från Pacman så sätts Pacmans riktning som en förbjuden väg
    if(ghostPattern==GhostPatterns["FLEE"])
    then
      if(pacmanDirection==Directions["UP"])
      then
        north1=false
        north2=false
      end    
      
      if(pacmanDirection==Directions["DOWN"])
      then
        south1=false
        south2=false
      end
      
      if(pacmanDirection==Directions["RIGHT"])
      then
        east1=false
        east2=false        
      end
      
      if(pacmanDirection==Directions["LEFT"])
      then
        west1=false
        west2=false        
      end      
    end
    
    --Om spöket är ute på patrul så ska det inte kunna tvärvända
    if(ghostPattern==GhostPatterns["PATROL"])
    then
      if(ghostDirection==Directions["DOWN"])
      then
        north1=false
        north2=false
      end    
      
      if(ghostDirection==Directions["UP"])
      then
        south1=false
        south2=false
      end
      
      if(ghostDirection==Directions["LEFT"])
      then
        east1=false
        east2=false        
      end
      
      if(ghostDirection==Directions["RIGHT"])
      then
        west1=false
        west2=false        
      end      
    end
    
  end
    
	--Sätter en ny riktning beroende på om det finns väggar i vägen eller inte.
	--Finns en liten risk att loopen tar för lång tid, om man har otur och den slumpar fel nummer
	while(directionSet==false)
	do  
    randomDirection=math.random(1,4)
		
		if(randomDirection==1)
		then
			randomDirection="Up"
		end
		
		if(randomDirection==2)
		then
			randomDirection="Down"
		end
		
		if(randomDirection==3)
		then
			randomDirection="Left"
		end
		
		if(randomDirection==4)
		then
			randomDirection="Right"
		end
		
		if(randomDirection=="Up" and forbiddenVerticalMovement~="Up")
		then
			if(north1==true and north2==true)
			then
				directionSet=true
			end
		end
		
		if(randomDirection=="Down" and forbiddenVerticalMovement~="Down")
		then
			if(south1==true and south2==true)
			then
				directionSet=true
			end
		end
		
		if(randomDirection=="Left" and forbiddenHorizontalMovement~="Left")
		then
			if(west1==true and west2==true)
			then
				directionSet=true
			end
		end
		
		if(randomDirection=="Right" and forbiddenHorizontalMovement~="Rigt")
		then
			if(east1==true and east2==true)
			then
				directionSet=true
			end
		end
	end
	
	return randomDirection
end



function SetNewPattern(ghostState, distanceToPacman)
	
  print("Sätter spökpattern")
  
  if(ghostState==GhostStates["DEAD"])
	then
		return GhostPatterns["GO_HOME"]
	end
	
  if(ghostState==GhostStates["WANDER"] and distanceToPacman<10)
  then
    return GhostPatterns["PURSUIT"]
  end
  
  if(ghostState==GhostStates["WANDER"])
  then
    return GhostPatterns["PATROL"]
  end
  
  if(ghostState==GhostStates["ELIMINATE"])
  then
    return GhostPatterns["PURSUIT"]
  end
  
  if(ghostState==GhostStates["SCARED"] and distanceToPacman<10)
  then
    return GhostPatterns["FLEE"]
  end
  
  if(ghostState==GhostStates["SCARED"])
  then
    return GhostPatterns["EVADE"]
  end

  print("Hamnar vi här har något gått fel!")

end


function SetNewState(pacmanState, ghostState, ghostX, ghostY, pipsLeft)

  print(ghostState)
  print("Sätter en ny state för spöket")

  if(ghostState~=GhostStates["DEAD"])
  then
    print("Spökets state är inte dead...")
    if(pacmanState==PacmanStates["INVINCIBLE"])
    then
      print("Pacman är invincible, spöket blir SCARED")
      return GhostStates["SCARED"]
    end
    
    if(pipsLeft<25 and pacmanState~=PacmanStates["INVINCIBLE"])
    then
      print("Det finns få pips kvar, spöket blir ELIMINATE")
      return GhostStates["ELIMINATE"]
    end
    
    if(pacmanState==PacmanStates["NORMAL"])
    then
      print("Pacman är normal, spöket går till WANDER")
      return GhostStates["WANDER"]
    end
  
  end

  if(ghostState==GhostStates["DEAD"] and ghostX==17 and ghostY==17)
  then    
    print("Spöket är dead och är i boet, återgår till WANDER")
    return GhostStates["WANDER"]
  end
  
  print("Inget av vilkoren är uppfyllda, återgår till ursprungsstatus")
  
  return ghostState

end

