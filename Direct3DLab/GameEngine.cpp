//Lua-bransch.

#include "DirectX.h"
#include "GameEngine.h"
#include "DirectInput.h"
#include "Sprite.h"
#include "MovingObject.h"
#include "StationaryObject.h"
#include "Pacman.h"
#include "Ghost.h"
#include "World.h"
#include "Timer.h"
#include <math.h>

//Lua-includes
extern "C"
{
	#include "lua.h"
	#include "lualib.h""
	#include "lauxlib.h"
}

//Grundl�ggande parameterar f�r om spelet �r ig�ng eller ej
//samt sk�rmstorlek. 
bool gameRunning = true;
int screenWidht = 576;
int screenHeight = 768;

//DirectX-objekt
DirectX *directX;
DirectInput *input;

Ghost  *ghost;
Pacman *pacman;
World *world;

Timer *powerUpTimer;

//Ljud, bilder, menyer etc


bool initGame(HWND window){

	//Laddar in sprites, s�tter upp input, directX etc

	srand((unsigned int)time(NULL)); //Skapar ett nytt slumpfr�

	directX = new DirectX(window);
	if (!directX)
		return false;

	input = new DirectInput(window);
	if (!input)
		return false;

	ShowCursor(true);

	pacman = new Pacman(directX);
	if (!pacman->GetSprite()->IsOK())
		return false;

	ghost = new Ghost(directX);
	if (!ghost->GetSprite()->IsOK()){
		return false;
	}

	world = new World(directX);
	for (int i = 0; i < world->GetWorldSize() - 1; i++){
		if (!world->GetWorldSegment(i)->GetSprite()->IsOK())
			return false;
	}

	//Timer som g�r spelaren od�dlig i fem sekunder
	powerUpTimer = new Timer(5000);

	return true;
}

bool detectBoxCollision(Sprite* spriteA, Sprite* spriteB){

	//Boxkollision, funktionen tar tv� sprites och r�knar ut en rektangel runt de tv�
	//och om de �verlappar s� returneras en krock. Den h�r funktionen fick jag skriva om fr�n
	//de f�rra projekten f�r den var inte tillr�ckligt precis.

	int aLeft = spriteA->getX();
	int aTop = spriteA->getY();
	int aRight = spriteA->getX() + spriteA->getWidth();
	int aBottom = spriteA->getY() + spriteA->getHeight();

	int bLeft = spriteB->getX();
	int bTop = spriteB->getY();
	int bRight = spriteB->getX() + spriteB->getWidth();
	int bBottom = spriteB->getY() + spriteB->getHeight();

	if (aBottom <= bTop)
		return false;

	if (aTop >= bBottom)
		return false;

	if (aRight <= bLeft)
		return false;

	if (aLeft >= bRight)
		return false;

	return true;
}

void renderGraphics(HWND window){

	//H�r ritas all grafik ut
	D3DCOLOR backColor = (0, 0, 0);
	D3DCOLOR fontcolor = (184, 255, 0);
	directX->clearScreen(backColor);
	directX->getDevice()->BeginScene();
	{
		world->Draw();
		pacman->Draw();
		ghost->Draw();
		//Ers�tts med lista �ver MovingObjects sedan n�r monster l�ggs till.

		D3DCOLOR fontColor = D3DCOLOR_XRGB(184, 255, 0);

		//Skriver ut liv och po�ng. 
		string scoreText = "Score: " + std::to_string(pacman->GetScore());
		directX->printText(scoreText.c_str(), 50, 650, fontColor);

		string livesText = "Lives: " + std::to_string(pacman->GetLives());
		directX->printText(livesText.c_str(), 350, 650, fontColor);
	}

	directX->getDevice()->EndScene();
	directX->getDevice()->Present(NULL, NULL, NULL, NULL);
}

void checkUserInput(HWND window){

	//H�r kontrolleras tangentbordstryck

	input->poll(window);

	if (input->keyDown(DIK_ESCAPE))
		gameRunning = false;

	if (input->keyDown(DIK_RIGHTARROW))
		pacman->SetDirection(RIGHT);

	if (input->keyDown(DIK_LEFTARROW))
		pacman->SetDirection(LEFT);

	if (input->keyDown(DIK_UPARROW))
		pacman->SetDirection(UP);

	if (input->keyDown(DIK_DOWNARROW))
		pacman->SetDirection(DOWN);
}

void DecideGhostStateFromScript(){

	//Best�mmer sp�kets State utfr�n scriptet GhosteStates.lua

	lua_State* state = luaL_newstate();
	luaL_openlibs(state);

	int result = luaL_dofile(state, "GhostStates.lua");

	if (result != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error loading Lua script file.", MB_OK);
	}

	lua_getglobal(state, "SetNewState");

	lua_pushinteger(state, (int)pacman->GetState());
	lua_pushinteger(state, (int)ghost->GetState());

	int ghostX = nearbyintf((ghost->GetSprite()->getX()) / 16);
	int ghostY = nearbyintf((ghost->GetSprite()->getY()) / 16);

	lua_pushinteger(state, ghostX);
	lua_pushinteger(state, ghostY);

	lua_pushinteger(state, world->GetPips());

	if (lua_pcall(state, 5, 1, 0) != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error executing Lua function SetNewState().", MB_OK);
	}
	else{

		int ghostState = lua_tointeger(state, -1);

		ghost->SetState((GhostState)ghostState);

	}

	lua_pop(state, 1);
	lua_close(state);
}

void DecideGhostPatternFromScript(){
	
	//Best�mmer sp�kets r�relsem�nster fr�n scriptet GhostPatterns.lua
	
	lua_State* state = luaL_newstate();
	luaL_openlibs(state);

	int result = luaL_dofile(state, "GhostPatterns.lua");

	if (result != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error loading Lua script file.", MB_OK);
	}

	lua_getglobal(state, "SetNewPattern");

	lua_pushinteger(state, (int)ghost->GetState());
	
	//Ber�knar avst�nd fr�n sp�ke till Pacman
	
	int ghostX = nearbyintf((ghost->GetSprite()->getX()) / 16);
	int ghostY = nearbyintf((ghost->GetSprite()->getY()) / 16);

	int pacmanX = nearbyintf((pacman->GetSprite()->getX()) / 16);
	int pacmanY = nearbyintf((pacman->GetSprite()->getY()) / 16);

	float a = pow(ghostX - pacmanX, 2);
	float b = pow(ghostY - pacmanY, 2);

	float distanceToPacman = sqrt(a+b);

	lua_pushinteger(state, distanceToPacman);

	if (lua_pcall(state, 2, 1, 0) != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error executing Lua function SetNewPattern().", MB_OK);
	}
	else{

		int pattern = lua_tointeger(state, -1);

		ghost->SetPattern((GhostPattern)pattern);
	}

	lua_pop(state, 1);
	lua_close(state);
}

void DecideGhostMovementFromScript(){

	//Vad som h�nder h�r �r att C++ skickar in information om sp�kets omv�rld till Lua-scriptet
	//Lua anv�nder sedan den informationen f�r att best�mma vad som ska h�nda
	//Finns det v�ggar �t ena h�llet s� kommer sp�ket inte �ka d�r�t
	//Befinner sig sp�ket i boet s� f�rs�ker det alltid l�mna det t ex
	//Det finns en bug h�r n�gonstans som g�r att sp�kets r�relsem�nster �r n�got ryckigt, den har jag inte lyckats
	//l�sa riktigt �n (verkar kicka in n�r spelaren b�rjar styra Pacman med tangentbordet)

	lua_State* state = luaL_newstate();
	luaL_openlibs(state);

	int result = luaL_dofile(state, "GhostDirections.lua");

	if (result != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error loading Lua script file.", MB_OK);
	}

	lua_getglobal(state, "SetNewDirection");

	//Avrundar sp�kets position till n�rmsta heltal
	int y = nearbyintf(ghost->GetSprite()->getY() / 16);
	int x = nearbyintf(ghost->GetSprite()->getX() / 16);


#pragma region WallOrNoWall

	//R�knar ut och skickar information till scriptet om det finns en v�gg eller inte i respektive riktning
	
	bool north = false, south = false, west = false, east = false;

	//North1
	if (world->GetWorldTile(x, y - 1) == EMPTY_SPACE 
		|| world->GetWorldTile(x, y - 1) == EMPTY_INTERSECTION_POINT 
		|| world->GetWorldTile(x, y - 1) == POWER_PILL
		|| world->GetWorldTile(x, y - 1) == INTERSECTION_POINT
		|| world->GetWorldTile(x, y - 1) == PIP_SPACE)
		north = true;


	//North2
	if (north){
		if (world->GetWorldTile(x + 1, y - 1) == EMPTY_SPACE
			|| world->GetWorldTile(x + 1, y - 1) == EMPTY_INTERSECTION_POINT
			|| world->GetWorldTile(x + 1, y - 1) == POWER_PILL
			|| world->GetWorldTile(x + 1, y - 1) == INTERSECTION_POINT
			|| world->GetWorldTile(x + 1, y - 1) == PIP_SPACE)
			north = true;
		else
			north = false;
	}
		
	//South1
	if (world->GetWorldTile(x, y + 1) == EMPTY_SPACE 
		|| world->GetWorldTile(x, y + 1) == EMPTY_INTERSECTION_POINT 
		|| world->GetWorldTile(x, y + 1) == POWER_PILL
		|| world->GetWorldTile(x, y + 1) == INTERSECTION_POINT
		|| world->GetWorldTile(x, y + 1) == PIP_SPACE)
		south = true;


	//South2
	if (south){
		if (world->GetWorldTile(x + 1, y + 2) == EMPTY_SPACE
			|| world->GetWorldTile(x + 1, y + 2) == EMPTY_INTERSECTION_POINT
			|| world->GetWorldTile(x + 1, y + 2) == POWER_PILL
			|| world->GetWorldTile(x + 1, y + 2) == INTERSECTION_POINT
			|| world->GetWorldTile(x + 1, y + 2) == PIP_SPACE)
			south = true;
		else
			south = false;
	}
	
	//West1
	if (world->GetWorldTile(x - 1, y) == EMPTY_SPACE 
		|| world->GetWorldTile(x - 1, y) == EMPTY_INTERSECTION_POINT 
		|| world->GetWorldTile(x - 1, y) == POWER_PILL
		|| world->GetWorldTile(x - 1, y) == INTERSECTION_POINT
		|| world->GetWorldTile(x - 1, y) == PIP_SPACE)
		west = true;
	
	//West2
	if (west){
		if (world->GetWorldTile(x - 1, y + 1) == EMPTY_SPACE
			|| world->GetWorldTile(x - 1, y + 1) == EMPTY_INTERSECTION_POINT
			|| world->GetWorldTile(x - 1, y + 1) == POWER_PILL
			|| world->GetWorldTile(x - 1, y + 1) == INTERSECTION_POINT
			|| world->GetWorldTile(x - 1, y + 1) == PIP_SPACE)
			west = true;
		else
			west = false;
	}
	
	//East1
	if (world->GetWorldTile(x + 2, y) == EMPTY_SPACE 
		|| world->GetWorldTile(x + 2, y) == EMPTY_INTERSECTION_POINT 
		|| world->GetWorldTile(x + 2, y) == POWER_PILL
		|| world->GetWorldTile(x + 2, y) == INTERSECTION_POINT
		|| world->GetWorldTile(x + 2, y) == PIP_SPACE)
		east = true;
	
	//East2
	if (east){
		if (world->GetWorldTile(x + 2, y + 1) == EMPTY_SPACE
			|| world->GetWorldTile(x + 2, y + 1) == EMPTY_INTERSECTION_POINT
			|| world->GetWorldTile(x + 2, y + 1) == POWER_PILL
			|| world->GetWorldTile(x + 2, y + 1) == INTERSECTION_POINT
			|| world->GetWorldTile(x + 2, y + 1) == PIP_SPACE)
			east = true;
		else
			east = false;
	}
	

#pragma endregion

	lua_pushinteger(state, x);
	lua_pushinteger(state, y);	

	lua_pushboolean(state, north);
	lua_pushboolean(state, south);
	lua_pushboolean(state, west);
	lua_pushboolean(state, east);
	
	lua_pushinteger(state, int(ghost->GetPattern()));

	lua_pushinteger(state, int(ghost->GetDirection()));

	//Avrundar Pacmans position till n�rmaste heltal
	int pacmanX = nearbyintf((pacman->GetSprite()->getX()) / 16);
	int pacmanY = nearbyintf((pacman->GetSprite()->getY()) / 16);

	lua_pushinteger(state, pacmanX);
	lua_pushinteger(state, pacmanY);

	lua_pushinteger(state, (int)pacman->GetDirection());

	lua_getglobal(state, "SetNewDirection");
	if (lua_pcall(state, 12, 1, 0) != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error executing Lua function SetNewDirection().", MB_OK);
	}
	else{
		
		int direction = lua_tointeger(state, -1);
		ghost->SetDirection((Direction)direction);

	}
	
	lua_pop(state, 1);
	lua_close(state);
}

void DecideGhostSpeedFromScript(){
	
	//S�tter sp�kets hastighet utifr�n scriptet GhostSpeeds.lua
	
	lua_State* state = luaL_newstate();
	luaL_openlibs(state);

	int result = luaL_dofile(state, "GhostSpeeds.lua");

	if (result != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error loading Lua script file.", MB_OK);
	}

	lua_getglobal(state, "SetNewSpeed");

	lua_pushinteger(state, (int)ghost->GetState());

	
	if (lua_pcall(state, 1, 1, 0) != 0){
		string luaError = lua_tostring(state, -1);
		MessageBox(NULL, luaError.c_str(), "Error executing Lua function SetNewSpeed().", MB_OK);
	}
	else{

		int newSpeed = lua_tointeger(state, -1);
		ghost->SetSpeed(newSpeed);
	}

	lua_pop(state, 1);
	lua_close(state);
}

void CheckForWalls(){

	//Kontrollerar om en v�gg har tr�ffats. D� forts�tter objektet i tidigare r�relse, om det �r en v�gg �ven d�r 
	//s� stoppar den. 

	bool walled = false;

	for (int i = 0; i < world->GetWorldSize(); i++){
		if (world->GetWorldSegment(i)->GetType() == WALL){
			if (detectBoxCollision(world->GetWorldSprite(i), pacman->GetSprite())){
				pacman->RestorePosition();
				pacman->SetDirection(pacman->GetPreviousDirection()); //Kolla en g�ng till om det �r krock. Om s� --> s�tt till idle
				walled = true;
				break;
			}
		}
	}

	if (walled){
		pacman->Move();
		for (int i = 0; i < world->GetWorldSize(); i++){
			if (world->GetWorldSegment(i)->GetType() == WALL){
				if (detectBoxCollision(world->GetWorldSprite(i), pacman->GetSprite())){
					pacman->SetDirection(IDLE);
					pacman->RestorePosition();
					break;
				}
			}
		}
	}

	//En smula reduant kod h�r, det har jag planer f�r att l�sa till n�sta inl�mning (dvs att man kollar
	//alla MovableObjects i en lista ist�llet. Men tiden fanns inte riktigt till nu.
	for (int i = 0; i < world->GetWorldSize(); i++){
		if (world->GetWorldSegment(i)->GetType() == WALL){
			if (detectBoxCollision(world->GetWorldSprite(i), ghost->GetSprite())){
				ghost->RestorePosition();
				ghost->SetDirection(ghost->GetPreviousDirection()); //Kolla en g�ng till om det �r krock. Om s� --> s�tt till idle
				walled = true;
				break;
			}
		}
	}

	if (walled){
		ghost->Move();
		for (int i = 0; i < world->GetWorldSize(); i++){
			if (world->GetWorldSegment(i)->GetType() == WALL){
				if (detectBoxCollision(world->GetWorldSprite(i), ghost->GetSprite())){

					ghost->Realign();
					break;
				}
			}
		}
	}


	//Jag har valt att l�gga ut punkter i spelkartan, f�r det finns t ex t-korsningar. De anv�nds f�r att 
	//sp�ket ska kunna v�lja en riktning d�r.

	for (int i = 0; i < world->GetIntersections(); i++){
		if (ghost->GetSprite()->getX() + 12 < world->GetIntersectionPoint(i)->xPos && ghost->GetSprite()->getX() + 18 > world->GetIntersectionPoint(i)->xPos){
			if (ghost->GetSprite()->getY() + 12 < world->GetIntersectionPoint(i)->yPos && ghost->GetSprite()->getY() + 18 > world->GetIntersectionPoint(i)->yPos){
				
				//F�rst kontrolleras/uppdateras sp�kets statusmaskin, sedan s�tts r�relsem�nstret och sist sj�lva r�relsen samt hastighet
				
				DecideGhostStateFromScript();
				DecideGhostPatternFromScript();
				DecideGhostMovementFromScript();
				DecideGhostSpeedFromScript();

			}
		}
	}
}

void EatObjects(){

	//Om Pacman springer p� ett object i v�rlden som g�r att �ta s� s�tts den till upp�ten och po�ng tilldelas.
	//Om Pacman �ter ett superpiller s� m�ste sp�kets state och hastighet uppdateras

	for (int i = 0; i < world->GetWorldSize(); i++){
		if (world->GetWorldSegment(i)->GetType() == PIP || world->GetWorldSegment(i)->GetType() == POWERUP){
			if (!world->GetWorldSegment(i)->IsEaten()){
				if (detectBoxCollision(world->GetWorldSprite(i), pacman->GetSprite())){
					world->GetWorldSegment(i)->SetEaten();
					pacman->AddScore(world->GetWorldSegment(i)->GetScore());
					if (world->GetWorldSegment(i)->GetType() == PIP)
						world->ReducePips();
					if (world->GetWorldSegment(i)->GetType() == POWERUP){
						pacman->SetInvincible(true);
						DecideGhostStateFromScript();
						DecideGhostSpeedFromScript();
						ghost->Realign();
						powerUpTimer->resetTimer();
					}
				}
			}
		}
	}
}

void CheckForCollisionWithMonster(){

	//Om Pacman krockar med ett monster s� d�r han s� l�nge han inte �r od�dlig.
	//***Beh�ver bytas ut mot en loop om fler monster l�ggs till***

	if (detectBoxCollision(pacman->GetSprite(), ghost->GetSprite())){
		if (pacman->GetState() != INVINCIBLE){
			pacman->ReduceLives();
			pacman->Reset();
		}
		else{
			if (pacman->GetState() == INVINCIBLE){
				pacman->AddScore(500); 
				ghost->SetState(DEAD); //Sp�ket state s�tts till d�d h�r, k�nns inte som att det �r n�dv�ndigt att det sk�ts i scriptet.				
			}
		}
	}
}

void MoveObjects(){

	pacman->Move();
	ghost->Move();
	if (ghost->GetDirection() == IDLE)
		ghost->Realign();

}

bool EndOfGame(){

	//Spelet tar slut om Pacman �ter alla sm�prickar eller om hans liv tar slut pga sp�kkrockar.

	if (pacman->GetLives() == 0){
		MessageBox(NULL, "Oh no! Blinky ate you!", "Game Over!", MB_OK);
		return true;
	}


	if (world->GetPips() == 0){
		MessageBox(NULL, "You've eaten all pips! Congrats!", "Game Over!", MB_OK);
		return true;
	}

	return false;
}

void CheckPowerUpTimer(){

	//Timer f�r od�dlighet kontrolleras h�r

	if (pacman->GetState() == INVINCIBLE){
		if (powerUpTimer->done()){
			pacman->SetInvincible(false);
			DecideGhostStateFromScript();
		}
	}
}

void gameLoop(HWND window){

	//Vid alla tillf�llen s� ritas grafik ut och anv�ndarens input kontrolleras alltid.
	checkUserInput(window);
	MoveObjects();
	CheckForWalls();
	EatObjects();
	CheckForCollisionWithMonster();
	CheckPowerUpTimer();
	renderGraphics(window);
	if (EndOfGame()){
		gameRunning = false;
	}


}

//H�r tas alla pekare bort som �r skapade med new.

void cleanUp(){

	delete powerUpTimer;
	delete pacman;
	delete ghost;
	delete world;

	input->directInputShutdown();
	delete input;
	directX->shutDown();
	delete directX;
}