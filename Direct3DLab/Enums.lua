GhostStates={
	WANDER=0,
	SCARED=1,
	DEAD=2,
	ELIMINATE=3
}

GhostPatterns={
	PATROL=0,
	PURSUIT=1,
	EVADE=2,
	FLEE=3,
	GO_HOME=4
}

PacmanStates={
  NORMAL=0,
	INVINCIBLE=1,
	EATEN=2
}

Directions={
  UP=0,
	DOWN=1,
	LEFT=2,
	RIGHT=3,
	IDLE=4
}