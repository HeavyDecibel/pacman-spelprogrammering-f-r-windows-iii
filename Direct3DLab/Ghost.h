#pragma once
#include "MovingObject.h"
#include "Enums.h"



class Ghost :
	public MovingObject
{


private:
	GhostState state;
	GhostPattern pattern;

public:
	
	Ghost(DirectX *directX);
	~Ghost();
	
	void SetSpeed(float ghostspeed);
	virtual void Draw() override;

	GhostState GetState();
	GhostPattern GetPattern();

	void SetState(GhostState newState);
	void SetPattern(GhostPattern newPattern);

	void Realign();

};

