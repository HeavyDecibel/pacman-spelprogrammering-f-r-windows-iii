math.randomseed(os.time())

function SetNewDirection(ghostX, ghostY, north1, north2, south1, south2, west1, west2, east1, east2)
	--K�r math.random() tre g�nger, annars kommer alltid samma slumptal genereras av n�gon anledning
	math.random()
	math.random()
	math.random()
	
	local randomDirection
	local directionSet=false
	
	--Om sp�ket befinner sig i boet s� slumpas inte riktningen utan den
	--l�mnar ist�llet boet. Annars kan det irra runt d�r i alla evighet
	if(ghostX==17 and ghostY==17)
	then
		randomDirection=1
		directionSet=true
	end
	
	
	--S�tter en ny riktning beroende p� om det finns v�ggar i v�gen eller inte.
	--Finns en liten risk att loopen tar f�r l�ng tid, om man har otur och den slumpar fel nummer
	while(directionSet==false)
	do
		randomDirection=math.random(1,4)
		
		if(randomDirection==1)
		then
			if(north1==99 or north1==91)
			then
				if(north2==99 or north2==91)
				then
					directionSet=true
				end
			end
		end
		
		if(randomDirection==2)
		then
			if(south1==99 or south1==91)
			then
				if(south2==99 or south2==91)
				then
					directionSet=true
				end
			end
		end
		
		if(randomDirection==3)
		then
			if(west1==99 or west1==91)
			then
				if(west2==99 or west2==91)
				then
					directionSet=true
				end
			end
		end
		
		if(randomDirection==4)
		then
			if(east1==99 or east1==91)
			then
				if(east2==99 or east2==91)
				then
					directionSet=true
				end
			end
		end
	end
	
	return randomDirection
end