function SetNewState(pacmanState, ghostState, ghostX, ghostY, pipsLeft)

  --Script som returnerar en ny status beroende på nuvarande status, Pacmans status och position enligt projektspecen

  dofile("Enums.lua")

  print(ghostState)
  print("Sätter en ny state för spöket")

  if(ghostState~=GhostStates["DEAD"])
  then
    print("Spökets state är inte dead...")
    if(pacmanState==PacmanStates["INVINCIBLE"])
    then
      print("Pacman är invincible, spöket blir SCARED")
      return GhostStates["SCARED"]
    end
    
    if(pipsLeft<25 and pacmanState~=PacmanStates["INVINCIBLE"])
    then
      print("Det finns få pips kvar, spöket blir ELIMINATE")
      return GhostStates["ELIMINATE"]
    end
    
    if(pacmanState==PacmanStates["NORMAL"])
    then
      print("Pacman är normal, spöket går till WANDER")
      return GhostStates["WANDER"]
    end
  
  end

  if(ghostState==GhostStates["DEAD"] and ghostX==17 and ghostY==17)
  then    
    if(pacmanState==PacmanStates["INVINCIBLE"])
    then
      --Det här är till för att spöket inte ska ge sig ut från boet så fort det når dit
      print ("Spöket är dead, Pacman är invincible. Spöket är fortfarande dött i boet")
      return GhostStates["DEAD"]
    else
      print("Spöket är dead och är i boet, Pacman är normal - återgår till WANDER")
      return GhostStates["WANDER"]
    end
  end
  
  print("Inget av vilkoren är uppfyllda, återgår till ursprungsstatus")
  
  return ghostState

end