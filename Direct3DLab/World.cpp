//Klassen som styr banans utseende och inneh�ll
//V�rlden ritas upp utifr�n en kommaseparerad fil. 

#include "World.h"
#include "Wall.h"
#include "Pip.h"
#include "PowerUp.h"


World::World(DirectX *directX):
directX(directX)
{
	pips = 0;
	ReadWorldFromFile();
	SetUpWorld();
}


World::~World()
{
	for (int i = 0; i < worldSegment.size(); i++)
		delete worldSegment[i];

	for (int i = 0; i < intersectionPoints.size(); i++)
		delete intersectionPoints[i];

}

void World::Draw(){

	for (int i = 0; i < worldSegment.size(); i++)
		worldSegment[i]->Draw();

}

void World::ReadWorldFromFile(){

	//Metod som l�ser in tilemapen fr�n en textfil
	//Jag valde CSV f�r d� kunde jag enkelt g�ra banan i Excel.

	string path = "World.csv";
	ifstream file(path);

	char nextChar;
	string number = "";
	int cols = 0;
	int rows = 0;

	if (!file.is_open()){
		MessageBox(NULL, "Mapfile not found.", "File not found error.", MB_OK);
		return;
	}
		

	if (file.is_open()){

		//Itererar igenom filen och plockar alla teckan fram till n�sta ; till en str�ng
		//som sedan l�ggs ner i tilemapen.
		while (file.get(nextChar)){
			if (nextChar > 47 && nextChar < 58){
				number += nextChar;
			}
			
			if (nextChar == ';'){
				tileMap[rows][cols] = atoi(number.c_str()); //G�r om teckent fr�n Ascii till vanliga siffror;
				number = "";
				rows++;
			}

			if (nextChar == '\n'){
				rows = 0;
				cols++;
			}
		}
		file.close();
	}
}

void World::SetUpWorld(){

	//Initierar v�rlden utifr�n tilemappen. 

	float xPos, yPos;

	for (int cols = 0; cols < WORLDHEIGHT; cols++){
		for (int rows = 0; rows < WORLDWIDTH; rows++){

			xPos = rows * 16;
			yPos = cols * 16;

			//Ritar ut v�ggar
			if (tileMap[rows][cols] < 90){
				worldSegment.push_back(new Wall(directX));
				worldSegment[worldSegment.size()-1]->SetPosition(xPos, yPos);
				worldSegment[worldSegment.size()-1]->SetFrame(tileMap[rows][cols]);
			}

			//Ritar ut "pips", s�nna d�r sm� po�ngpluppar som finns i Pacman. Det ritas ut mellan fyra tomma rutor i v�rldskartan.
			if (tileMap[rows][cols] > 95){
				if (tileMap[rows + 1][cols] > 95){
					if (tileMap[rows + 1][cols + 1] > 95){
						if (tileMap[rows][cols + 1] > 95){
							worldSegment.push_back(new Pip(directX));
							worldSegment[worldSegment.size() - 1]->SetPosition(xPos + 15, yPos + 15);
							pips++;
						}
					}
				}
			}

			//Ritar ut PowerUps som i senare labbar ska g�ra spelaren od�dlig.
			if (tileMap[rows][cols] == 97){
				worldSegment.push_back(new PowerUp(directX));
				worldSegment[worldSegment.size() - 1]->SetPosition(xPos + 6, yPos + 6);	
			}

			//Fyller lista med beslutspunkter f�r korsningar
			if (tileMap[rows][cols] == 98 || tileMap[rows][cols] == 92){
				intersectionPoints.push_back(new IntersectionPoint(xPos + 16, yPos + 16));
				
				/*
				IntersectionPoint* intTmp = new IntersectionPoint(xPos + 16, yPos + 16);
				string output = std::to_string(intTmp->xPos) + "," + std::to_string(intTmp->yPos);
				MessageBox(NULL, output.c_str(), "POS", MB_OK);
				delete intTmp;
				*/
			}
		}
	}
}


//Returnerar privata medlemmar. 
Sprite* World::GetWorldSprite(int i){
	return worldSegment[i]->GetSprite();
}

int World::GetWorldSize(){
	return worldSegment.size();
}

StationaryObject* World::GetWorldSegment(int i){
	return worldSegment[i];
}

IntersectionPoint* World::GetIntersectionPoint(int i){
	return intersectionPoints[i];
}

int World::GetIntersections(){
	return intersectionPoints.size();
}

int World::GetWorldTile(int x, int y){
	return tileMap[x][y];
}

int World::GetPips(){
	return pips;
}

void World::ReducePips(){
	pips--;
}