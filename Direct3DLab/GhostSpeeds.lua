function SetNewSpeed(ghostState)

  --Script som sätter spökets hastighet beroende på state

  dofile("Enums.lua")

  --Spökets standardhastighet är 1.5
  local NewSpeed=1.5
  
  --Om spöket är död och på väg hem till boet så är det snabbare än vanligt
  if(ghostState==GhostStates["DEAD"])
  then
    NewSpeed=8.0
  end
  
  --För att göra det lite svårare så blir spöket snabbare mot slutet av spelet
  if(ghostState==GhostStates["ELIMINATE"])
  then
    NewSpeed=3.0
  end
  
  return NewSpeed
end