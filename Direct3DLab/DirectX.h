#pragma once

#ifndef __DIRECTX_H_INCLUDED__
#define __DIRECTX_H_INCLUDED__

#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include "GameEngine.h"

//Bibliotek f�r DirectX (version 9 d� det �r samma som t�cks i kurslitteraturen)
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dsound.lib") //Anv�nds f�r CWaveFile i DSUtil.h
#pragma comment(lib, "winmm.lib") //Anv�nds f�r CWaveFile i DSUtil.h
#pragma comment(lib, "dxerr.lib") //Anv�nds f�r CWaveFile i DSUtil.h

using namespace std;

class DirectX{

private:

	LPDIRECT3D9 direct3DInterface; //Gr�nsnittet (chefen �ver de �vriga objekten)
	LPDIRECT3DDEVICE9 direct3DDevice; //DX-enheten
	LPDIRECT3DSURFACE9 surfaceBackbuffer; //Backbufferytan
	LPD3DXFONT defaultFont;
	bool transparancy; //<-Best�mmer om transparance ska vara p� eller inte

	bool direct3DInit(HWND window); //<-Initierar DirectX
	void setMaxResolution(); //R�knar ut sk�rmens maxresolution (eller iaf vad Windows �r inst�llt p�, det beh�ver ju egentligen inte vara samma)
	bool setParameters(HWND window, int width, int height); //<-St�ller in parametrar f�r DirectX
	void initFonts();

public:

	DirectX(HWND window); //Konstruktor
	~DirectX(); //Destruktor

	void clearScreen(D3DCOLOR color); //Rensa sk�rmen till valfri f�rg
	void loadImageFromFile(const char* filename); //Ladda bilder
	void drawImageToScreen(); //Rita ut bilden h�r (ta emot en bildparameter)
	void printText(const char* text, float x, float y, D3DCOLOR fontColor); //Rita ut text p� sk�rmen
	void setTransparancy(); //V�lja genomskinlighet p�/av
	bool getTransparancy(); //Returnerar transparensboolen
	void scaleImage(); //Rita ut bilder med godtycklig storlek oavsett bildens dimensioner (skalning)
	LPDIRECT3DDEVICE9 getDevice(); //Returnerar DirectX-enheten
	LPDIRECT3DSURFACE9 getBackbuffer(); //Backbufferytan

	void shutDown(); //St�nger ner DirectX
	bool resetResoution(HWND window, int width, int height); //St�ll in uppl�sningen enligt parametrarna
	void printText(const char* text, std::string font, int size, float x, float y);
	LPDIRECT3DTEXTURE9 loadTexture(string filename, D3DCOLOR transcolor);
	LPDIRECT3DSURFACE9 loadSurface(string filename);

};

#endif
