#pragma once

#ifndef __GAMEENGINE_H_INCLUDED__
#define __GAMEENGINE_H_INCLUDED__

//#include<vld.h> //Visual Leak Detector

#include <vector>
#include <time.h>
#include "DirectX.h"
#include "DirectInput.h"

extern bool gameRunning;
extern int screenWidht;
extern int screenHeight;

void gameLoop(HWND window);
bool initGame(HWND window);
void cleanUp();

#endif