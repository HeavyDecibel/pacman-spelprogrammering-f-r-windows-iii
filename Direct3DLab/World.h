#pragma once

#include <fstream>
#include <string>
#include <Windows.h>
#include <vector>

using namespace std;

class StationaryObject;
class Wall;
class DirectX;
class Sprite;

struct IntersectionPoint{

	int xPos, yPos;

	IntersectionPoint(int x, int y){
		xPos = x;
		yPos = y;
	}
};

class World
{
public:	
	
	//Storleke p� spelplanen
	static const int WORLDWIDTH = 36;
	static const int WORLDHEIGHT = 39;

	World(DirectX *directX);
	~World();

	Sprite* GetWorldSprite(int i);
	StationaryObject* GetWorldSegment(int i);
	IntersectionPoint* GetIntersectionPoint(int i);

	void Draw();
	
	int GetWorldTile(int x, int y);
	int GetWorldSize();
	int GetIntersections();
	int GetPips();
	void ReducePips();

private:

	vector<IntersectionPoint*> intersectionPoints;
	vector<StationaryObject*> worldSegment;
	DirectX *directX;

	int tileMap[WORLDWIDTH][WORLDHEIGHT];
	int pips;

	void ReadWorldFromFile();
	void SetUpWorld();
		
};

