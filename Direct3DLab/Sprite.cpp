/*

Klass f�r utritande och animering av sprites

*/

#include "Sprite.h"
#include "Timer.h"
#include "DirectX.h"

//*************************PRIVATE*******************************

bool Sprite::createSprite(string filename){

	//Laddar textur fr�n en bildfil och knyter sedan ihop den med spriteobjektet

	this->texture = directX->loadTexture(filename, (255, 0, 255));
	if (!texture){
		return false;
	}
		
	D3DXCreateSprite(directX->getDevice(), &sprite);
	if (!sprite){
		return false;
	}
		

	return true;

}


void Sprite::createTimer(){

	//Timer som styr mellanrummet mellan animeringarna i en sprite

	animationTimer = new Timer(75);

}

//*************************PUBLIC*******************************

Sprite::Sprite()
{
	//Alternativ konstruktor som �r tom. Om man vill ha en dummy sprite

	createTimer();

}


Sprite::Sprite(int width, int height, int mapLeft, int mapTop, float x, float y, int noFrames, int rows, int cols, DirectX *directX, string filename) :
width(width),
height(height),
mapLeft(mapLeft),
mapTop(mapTop),
x(x),
y(y),
frame(0),
rows(rows),
columns(cols),
noFrames(noFrames),
filename(filename),
directX(directX){

	//Konstruktor som blivit lite v�l ut�kad och tar lite f�r m�nga parametrar nu. 
	//I en perfekt v�rld hade jag gjort flera �verladdade konstruktorer ist�llet, men nu f�r vi leva med det :-)

	createdOK = true;

	if (!createSprite(filename))
		createdOK = false;
	
	this->map = { mapLeft, mapTop, mapLeft + width*noFrames, mapTop + width*noFrames };
	//Map �r rektangeln i bildfilen, just nu s� f�ruts�tter den att alla sprites �r av samma storlek.
	//Det beh�ver n�dv�ndigtvis inte vara sant i ett annat projekt och d� beh�ver den g�ras om
	scaling = 1.0f; //Initierar skalan till 1.0 vid skapandet av spriten.
	rotate = 0; //S�tter rotationsniv�n till 0, s� att upp �r upp vid start
	transparency = 255; //Niv�n p� genomskinlighet �r solid vid skapande. H�r utg�r jag fr�n att alla sprites har en alfakanal som g�r dess bakgrund transparent
	createTimer();

}


Sprite::~Sprite()
{
	delete animationTimer;
}

bool Sprite::IsOK(){
	return createdOK;
}

void Sprite::recreateSprite(){

	//Om spriten, av n�gon anledning, beh�ver skapas om anropas den h�r funktionen
	createSprite(filename);

}

void Sprite::drawSprite(){

	//Ritar ut spriten p� sk�rmen. 

	//H�r nedan skapas en matris upp inneh�llande v�rden f�r
	//position, skalning och mittpunkt (f�r att kunna rotera spriten)
	D3DXMATRIX matrix;
	D3DXVECTOR2 trans(x, y);
	D3DXVECTOR2 scale(scaling, scaling);
	D3DXVECTOR2 center(float(width*scaling) / 2, float(height*scaling) / 2);
	D3DXMatrixTransformation2D(&matrix, NULL, 0, &scale, &center, rotate, &trans);
	sprite->SetTransform(&matrix);

	if (directX->getTransparancy())
		sprite->Begin(D3DXSPRITE_ALPHABLEND);
	else
		sprite->Begin(NULL);

	RECT currentFrame = { mapLeft + (frame*width), mapTop, mapLeft + (frame*width) + width, mapTop + height };
	sprite->Draw(this->texture, &currentFrame, NULL, NULL, D3DCOLOR_RGBA(255, 255, 255, this->transparency));
	sprite->End();
}

void Sprite::drawSpriteV2(){

	D3DXMATRIX matrix;
	D3DXVECTOR2 trans(x, y);
	D3DXVECTOR2 scale(scaling, scaling);
	D3DXVECTOR2 center(float(width*scaling) / 2, float(height*scaling) / 2);
	D3DXMatrixTransformation2D(&matrix, NULL, 0, &scale, &center, rotate, &trans);
	sprite->SetTransform(&matrix);

	if (directX->getTransparancy())
		sprite->Begin(D3DXSPRITE_ALPHABLEND);
	else
		sprite->Begin(NULL);

	//RECT currentFrame = { mapLeft + (frame*width), mapTop, mapLeft + (frame*width) + width, mapTop + height };
	RECT currentFrame;
	currentFrame.left = (frame%columns)*width;
	currentFrame.top = (frame / columns)*height;
	currentFrame.right = currentFrame.left + width;
	currentFrame.bottom = currentFrame.top + height;

	sprite->Draw(this->texture, &currentFrame, NULL, NULL, D3DCOLOR_RGBA(255, 255, 255, this->transparency));
	sprite->End();

}


void Sprite::setNewPos(float x, float y){

	//Byter spritens x- och y-koordinater

	this->x = x;
	this->y = y;
}

void Sprite::rotateSprite(float degree){

	//Tar emot grader som omvandlas till radianer som i sin tur anv�nds f�r att r�kna ut roteringen p� spriten

	rotate = degree*3.1415926535 / 180.0;;
}

void Sprite::setTransparencyLevel(int transparency){
	//S�tter transparensen
	this->transparency = transparency;
}

void Sprite::scaleSprite(float scale){
	//S�tter ny skala

	this->scaling += scale;
}

void Sprite::animateSprite(){

	if (animationTimer->done()){
		frame += 1;
		if (frame > noFrames)
			frame = 0;
	}
}

//TODO:L�gg in funktioner f�r h�ger, v�nster, upp�t och ned�t

void Sprite::moveSpriteX(float movement){
	x += movement;
}

void Sprite::moveSpriteY(float movement){

	y += movement;

}

void Sprite::moveLeft(float movement){


	x = x - movement;
}

void Sprite::moveRight(float movement){

	x = x + movement;

}


void Sprite::moveUp(float movement){

	y = y - movement;

}


void Sprite::moveDown(float movement){

	y = y + movement;

}


float Sprite::getX(){
	return x;
}

float Sprite::getY(){
	return y;
}

int Sprite::getWidth(){
	return width;
}

int Sprite::getHeight(){
	return height;
}


void Sprite::setDirection(int direction){
	this->direction = direction;
}

int Sprite::getDirection(){
	return this->direction;
}

void Sprite::setFrame(int frame){
	this->frame = frame;
}

void Sprite::setTimer(int time){
	animationTimer->setNewDelay(time);
}

int Sprite::getFrame(){

	return frame;
}

int Sprite::getNoFrames(){
	return noFrames;
}