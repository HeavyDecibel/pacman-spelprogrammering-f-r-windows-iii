#include "PowerUp.h"
#include "Sprite.h"


PowerUp::PowerUp(DirectX *directX) :StationaryObject(directX)
{
	type = POWERUP;
	sprite = new Sprite(20, 20, 0, 0, 0, 0, 1, 1, 1, directX, "Graphics\\PowerUp.png");
	scoreValue = 50;
}


PowerUp::~PowerUp()
{
}

