#pragma once

#include "Enums.h"

class Sprite;
class Timer;
class DirectX;


class MovingObject
{
protected:
	Sprite *sprite;
	DirectX *directX;
	float speed;
	float tmpX, tmpY;
	Direction direction;
	Direction previousDirection;
	

public:
	MovingObject(DirectX *directX);
	~MovingObject();
	
	virtual void Move();
	void SetDirection(Direction direction);

	Direction GetDirection();
	Direction GetPreviousDirection();

	virtual void SetNewPosition(float x, float y);
	void RestorePosition();
	
	virtual void Draw();

	Sprite *GetSprite();

};

