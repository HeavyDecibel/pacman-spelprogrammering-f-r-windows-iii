#pragma once

class Sprite;
class DirectX;

//Enums f�r vilken typ av station�rt objekt det �r.
enum StationaryType{
	WALL,
	PIP,
	POWERUP,
	FRUIT
};

class StationaryObject
{
protected:
	DirectX *directX;
	Sprite *sprite;
	StationaryType type;
	bool eaten;
	int scoreValue;

public:
	StationaryObject(DirectX *directX);
	~StationaryObject();

	void Draw();
	StationaryType GetType();
	Sprite *GetSprite();

	void SetFrame(int frame);
	void SetPosition(float x, float y);
	void SetEaten();
	int GetScore();
	bool IsEaten();

};

