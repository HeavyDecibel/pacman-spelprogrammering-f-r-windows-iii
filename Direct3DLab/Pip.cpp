#include "Pip.h"
#include "Sprite.h"

Pip::Pip(DirectX *directX) :StationaryObject(directX)
{
	type = PIP;
	sprite = new Sprite(4, 4, 0, 0, 0, 0, 1, 1, 1, directX, "Graphics\\pip.png");
	scoreValue = 10;
}


Pip::~Pip()
{
}
