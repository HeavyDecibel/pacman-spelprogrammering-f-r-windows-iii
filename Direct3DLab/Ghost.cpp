#include "Ghost.h"
#include "Sprite.h"

Ghost::Ghost(DirectX *directX) :MovingObject(directX)
{
	sprite = new Sprite(30, 30, 0, 0, 273, 273, 12, 3, 4, directX, "Graphics\\BlinkySpriteSheetv2.png"); //Startposition	

	//St�ller in sp�kets startv�rden
	state = WANDER;
	pattern = PATROL;
	direction = IDLE;
}


Ghost::~Ghost()
{
}


void Ghost::Draw(){

	//�verskuggad version av MovingObjects Draw. R�tt frame i spritesheeten v�ljs ut.

	int frame=0;

	switch (direction){
	case UP:
		if (state == WANDER || state == ELIMINATE)
			frame = 3;
		if (state == DEAD)
			frame = 7;
		break;
	case DOWN:
		if (state == WANDER || state == ELIMINATE)
			frame = 2;
		if (state == DEAD)
			frame = 6;
		break;
	case LEFT:
		if (state == WANDER || state == ELIMINATE)
			frame = 0;
		if (state == DEAD)
			frame = 4;
		break;
	case RIGHT:
		if (state == WANDER || state == ELIMINATE)
			frame = 1;
		if (state == DEAD)
			frame = 5;
		break;
	case IDLE:
		if (state == WANDER || state == ELIMINATE)
			frame = 0;
		if (state == DEAD)
			frame = 4;
		break;
	}

	if (state == SCARED)
		frame = 8;

	sprite->setFrame(frame);

	MovingObject::Draw();
}

void Ghost::SetSpeed(float ghostspeed){
	speed = ghostspeed;
}

GhostPattern Ghost::GetPattern(){
	return pattern;
}

GhostState Ghost::GetState(){
	return state;
}

void Ghost::SetState(GhostState newState){
	state = newState;
}

void Ghost::SetPattern(GhostPattern newPattern){
	pattern = newPattern;
}

void Ghost::Realign(){
	
	//Finns vissa l�gen d� sp�ket beh�ver s�ttas tillbaka i mitten av labyrintdelen
	
	int y = nearbyintf(sprite->getY() / 16);
	int x = nearbyintf(sprite->getX() / 16);

	SetNewPosition((x * 16) + 1, (y * 16) + 1);
}