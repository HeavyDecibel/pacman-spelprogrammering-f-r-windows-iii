//Grundklass f�r allting som kan r�ra sig, dvs Pacman och alla monster

#include "MovingObject.h"
#include "Sprite.h"
#include "Timer.h"


MovingObject::MovingObject(DirectX *directX):
directX(directX)
{
	//Alla r�rliga objekt har tv� riktningar, en nuvarande riktning och en tidigare. Dessa anv�nds vid kontroll
	//av krock med v�gg.
	
	direction = IDLE;
	previousDirection = IDLE;
}


MovingObject::~MovingObject()
{
	delete sprite;
}

void MovingObject::Move(){
	
	tmpX = sprite->getX();
	tmpY = sprite->getY();

	//Flyttar objektet �t det h�llet den �r sagd att g�
	switch (direction){
		case UP:
			sprite->setNewPos(sprite->getX(), sprite->getY() - speed);
			break;
		case DOWN:
			sprite->setNewPos(sprite->getX(), sprite->getY() + speed);
			break;
		case LEFT:
			sprite->setNewPos(sprite->getX() - speed, sprite->getY());
			break;
		case RIGHT:
			sprite->setNewPos(sprite->getX() + speed, sprite->getY());
			break;
	}

	//Kolla om n�got objekt har rymt utanf�r spelet (vilket man kan g�ra om man l�mnar labyrinten p� sidorna)
	//Kolla v�nsterkanten
	if (sprite->getX() < (0 - sprite->getWidth())){
		SetNewPosition(546, sprite->getY());
	}

	//Kolla h�gerkanten
	if (sprite->getX() > (576)){
		SetNewPosition(0 - sprite->getWidth(), sprite->getY());
	}
}

void MovingObject::Draw(){
	
	//Ritar ut och animerar spriten
	
	//sprite->animateSprite();
	sprite->drawSpriteV2();
}

void MovingObject::SetDirection(Direction direction){
	//S�tter riktning f�r r�relse
	previousDirection = this->direction;
	this->direction = direction;
	
}

Sprite* MovingObject::GetSprite(){
	return sprite;
}

Direction MovingObject::GetDirection(){
	return direction;
}

void MovingObject::SetNewPosition(float x, float y){
	sprite->setNewPos(x, y);
}

void MovingObject::RestorePosition(){
	//�terst�ller till f�reg�ende position om en krock med v�gg uppt�cks
	sprite->setNewPos(tmpX, tmpY);
}

Direction MovingObject::GetPreviousDirection(){
	return previousDirection;
}