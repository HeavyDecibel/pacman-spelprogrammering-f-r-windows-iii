//Klass som tar systemtiden och r�knar tills den n�tt tiden f�r delay
//Praktisk f�r bl a animeringar

#include "Timer.h"


Timer::Timer(int delay) :delay(delay)
{
	time = GetTickCount(); //S�tter time till nuvarande tid
}


Timer::~Timer()
{

}

bool Timer::done(){

	//Kollar om timern k�rt klart, om den gjort det s� nollst�lls den och b�rjar om

	int tmpTime = GetTickCount() - time;
	if (tmpTime > delay){
		time = GetTickCount();
		return true;
	}

	return false;
}

int Timer::getTime(){

	//Returnerar hur l�nge timern har k�rt.

	int tmpTime = GetTickCount() - time;
	return tmpTime;
}

void Timer::resetTimer(){

	//Nollst�ller timern

	time = GetTickCount();
}

void Timer::setNewDelay(int time){

	//S�tter ett nytt nedr�kningsv�rde

	delay = time;

}