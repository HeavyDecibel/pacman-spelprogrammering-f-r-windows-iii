#include <string>
#include "GameEngine.h"


using namespace std;

const string GAMETITLE = "Inl�mning - Complete Project";

LRESULT WINAPI winProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam){
	//Hanterare f�r Windowsmeddelanden, kommer egentligen bara att reagera p� WM_DESTROY d�
	//�vrigt hanteras p� andra st�llen av DirectInput

	switch (message){

	case WM_CLOSE:
		gameRunning = false;
		DestroyWindow(window);

	case WM_DESTROY:
		gameRunning = false; //Anv�nd den h�r f�r att kontrollera om spelet k�rs eller inte. K�rs det inte s� kan programmet avslutas
		PostQuitMessage(0);
		return 0;
	}

	//Skickar tillbaka meddelanden till windows
	return DefWindowProc(window, message, wParam, lParam);
}

WNDCLASSEX prepareWindow(HINSTANCE hInstance){
	WNDCLASSEX gameWindowClass;

	gameWindowClass.cbSize = sizeof(WNDCLASSEX);
	gameWindowClass.style = CS_HREDRAW | CS_VREDRAW; //<--Ritar om f�nstret om storleken f�r�ndras i h�jd- eller sidled eller om f�nstret flyttas
	gameWindowClass.lpfnWndProc = (WNDPROC)winProc; //<--Meddelandehanteraren f�r f�nstret

	gameWindowClass.cbClsExtra = 0;
	gameWindowClass.cbWndExtra = 0; //Extra minnesallokering

	gameWindowClass.hInstance = hInstance; //<--F�nstrets ID fr�n Windows

	gameWindowClass.hIcon = NULL;
	gameWindowClass.hIconSm = NULL; //<--Ikoner, har inte skapat n�gra f�r det h�r projektet

	gameWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW); //<--Muspekare
	gameWindowClass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH); //<--Bakgrund, inte s� viktigt eftersom DirectX sk�ter det h�r

	gameWindowClass.lpszMenuName = NULL; //<--Dropmeny, �r dock inte aktuellt f�r det h�r projektet

	gameWindowClass.lpszClassName = GAMETITLE.c_str(); //<--Namnet p� klassen f�r f�nstret

	return gameWindowClass;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPSTR lpCmdLine, int nShowCmd){


	//F�nsterklass registeras
	RegisterClassEx(&prepareWindow(hInstance));

	//F�nster och ett handtag till det skapas
	HWND gameWindow = CreateWindow(GAMETITLE.c_str(), GAMETITLE.c_str(), WS_SYSMENU | WS_BORDER | WS_CAPTION | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, screenWidht, screenHeight, NULL, NULL, hInstance, NULL);

	//Om gameWindow f�r ett ID p� 0 har n�gonting g�tt fel och programmet avslutas
	if (gameWindow == 0){
		MessageBox(NULL, "Error creating window, terminating program", GAMETITLE.c_str(), MB_ICONERROR);
		return 0;
	}

	//Visa och uppdatera f�nstret

	ShowWindow(gameWindow, SW_NORMAL);
	UpdateWindow(gameWindow);

	//S�tter upp spelparametrarna, om det returneras falskt s� har n�got snett.
	if (!initGame(gameWindow)){
		MessageBox(NULL, "Error initializing game, terminating progam...", GAMETITLE.c_str(), MB_ICONERROR);
		return 0;
	}

	MSG windowsMessage; //<-Windows meddelandestruct

	while (gameRunning){
		if (PeekMessage(&windowsMessage, NULL, 0, 0, PM_REMOVE)){
			TranslateMessage(&windowsMessage);
			DispatchMessage(&windowsMessage);
		}

		gameLoop(gameWindow); //Huvudloop f�r DirectX
	}

	cleanUp();


	return windowsMessage.wParam; //Returnerar meddelande tillbaka till Windows vid avslut
}

