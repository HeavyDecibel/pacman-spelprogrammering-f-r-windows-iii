#pragma once

#ifndef __TIMER_H_INCLUDED__
#define __TIMER_H_INCLUDED__

#include <Windows.h>

class Timer
{
private:
	int time;
	int delay;

public:
	Timer(int delay); //Konstruktor som tar hur l�nge timern ska k�ras
	~Timer();

	bool done(); //Kollar om timern �r klar
	int getTime();
	void resetTimer();
	void setNewDelay(int time);

};

#endif
