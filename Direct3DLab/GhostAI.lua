math.randomseed(os.time())

direction={}
direction[1]=true
direction[2]=false
direction[3]=false
direction[4]=false

function SetWalls(north, south, west, east)
	direction[1]=north
	direction[2]=south
	direction[3]=west
	direction[4]=east
end

function TestDirection(i)
	
	if(direction[i]==true) 
	then
		print("Wall")
	else
		print("Empty")
	end
end

function printX(x)
	print(x)
end

function RandomizeDirection()
	math.random()
	math.random()
	math.random()
	
	local randomNumber
	local DirectionSet=false
	while(DirectionSet==false)
	do
		randomNumber=math.random(1,4)
		if(direction[randomNumber]==false)
		then
			print("Cannot go that way")
			print(randomNumber)
		else
			DirectionSet=true
		end
		
		if(DirectionSet==true)
		then
			print("I can go here!")
			print(randomNumber)
		end
	end
	
	return randomNumber	
end

function Randomize()
	local randomNumber=math.random(1,4)
	print(randomNumber)
end